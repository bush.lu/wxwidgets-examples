# wxWidgets-examples

自我練習的例子
*  wxHello - 驗証開發環境，使用Code::Block 生成 wxWidgts Dialog 應用
*  wxSocketServer-blocking - 使用 wxSocketServer 直接等待連線，blocking GUI 的版本
*  wxSocketServer-nonblocking-singleclient - 使用 wxSocketServer 加上 event drive 的版本
*  wxSocketServer-nonblocking-multiclient - 加入使用 wxList 來記錄已連線 Clients 的版本
*  wxCustEventApp - 自定義 event 來傳遞資籽

(…待續)