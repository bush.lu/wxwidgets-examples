/***************************************************************
 * Name:      WxHelloApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2020-03-11
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef WXHELLOAPP_H
#define WXHELLOAPP_H

#include <wx/app.h>

class WxHelloApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // WXHELLOAPP_H
