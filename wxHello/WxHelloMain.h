/***************************************************************
 * Name:      WxHelloMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2020-03-11
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef WXHELLOMAIN_H
#define WXHELLOMAIN_H

//(*Headers(WxHelloDialog)
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/statline.h>
#include <wx/stattext.h>
//*)

class WxHelloDialog: public wxDialog
{
    public:

        WxHelloDialog(wxWindow* parent,wxWindowID id = -1);
        virtual ~WxHelloDialog();

    private:

        //(*Handlers(WxHelloDialog)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(WxHelloDialog)
        static const long ID_STATICTEXT1;
        static const long ID_BUTTON1;
        static const long ID_STATICLINE1;
        static const long ID_BUTTON2;
        //*)

        //(*Declarations(WxHelloDialog)
        wxBoxSizer* BoxSizer1;
        wxBoxSizer* BoxSizer2;
        wxButton* Button1;
        wxButton* Button2;
        wxStaticLine* StaticLine1;
        wxStaticText* StaticText1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // WXHELLOMAIN_H
