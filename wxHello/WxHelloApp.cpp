/***************************************************************
 * Name:      WxHelloApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2020-03-11
 * Copyright:  ()
 * License:
 **************************************************************/

#include "WxHelloApp.h"

//(*AppHeaders
#include "WxHelloMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(WxHelloApp);

bool WxHelloApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	WxHelloDialog Dlg(0);
    	SetTopWindow(&Dlg);
    	Dlg.ShowModal();
    	wxsOK = false;
    }
    //*)
    return wxsOK;

}
