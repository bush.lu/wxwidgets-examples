/***************************************************************
 * Name:      wxCustEventAppApp.cpp
 * Purpose:   Code for Application Class
 * Author:    bradlu ()
 * Created:   2020-04-02
 * Copyright: bradlu ()
 * License:
 **************************************************************/

#include "wxCustEventAppApp.h"

//(*AppHeaders
#include "wxCustEventAppMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(wxCustEventAppApp);

bool wxCustEventAppApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	wxCustEventAppFrame* Frame = new wxCustEventAppFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
