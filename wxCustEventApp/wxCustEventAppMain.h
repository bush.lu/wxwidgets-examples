/***************************************************************
 * Name:      wxCustEventAppMain.h
 * Purpose:   Defines Application Frame
 * Author:    bradlu ()
 * Created:   2020-04-02
 * Copyright: bradlu ()
 * License:
 **************************************************************/

#ifndef WXCUSTEVENTAPPMAIN_H
#define WXCUSTEVENTAPPMAIN_H

#include <wx/wx.h>
#include <wx/event.h>

//(*Headers(wxCustEventAppFrame)
#include <wx/button.h>
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/sizer.h>
#include <wx/statusbr.h>
#include <wx/textctrl.h>
//*)

#define DEF_MYPACKET_DATA_SIZE  (50) /* bytes */
typedef struct _DEF_MYPACKET_TYPE
{
    uint8_t data[DEF_MYPACKET_DATA_SIZE];
    uint8_t length;
} DEF_MYPACKET_TYPE;

class MyPacketEvent;

class wxCustEventAppFrame: public wxFrame
{
    public:

        wxCustEventAppFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~wxCustEventAppFrame();

    private:

        //(*Handlers(wxCustEventAppFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnBtnEvt1Click(wxCommandEvent& event);
        void OnBtnEvt2Click(wxCommandEvent& event);
        void OnBtnEvtAbcClick(wxCommandEvent& event);
        void OnBtnEvtDEFClick(wxCommandEvent& event);
        void OnBtnEvtPktClick(wxCommandEvent& event);
        //*)

        //(*Identifiers(wxCustEventAppFrame)
        static const long ID_BUTTON1;
        static const long ID_BUTTON2;
        static const long ID_BUTTON3;
        static const long ID_BUTTON4;
        static const long ID_TEXTCTRL2;
        static const long ID_BUTTON5;
        static const long ID_BUTTON6;
        static const long ID_TEXTCTRL1;
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)
        static const long ID_MY_CUST_NUM;
        static const long ID_MY_CUST_STR;
        static const long ID_MY_CUST_PKT;

        //(*Declarations(wxCustEventAppFrame)
        wxButton* BtnEvt1;
        wxButton* BtnEvt2;
        wxButton* BtnEvtAbc;
        wxButton* BtnEvtDEF;
        wxButton* BtnEvtPkt;
        wxButton* BtnGen;
        wxStatusBar* StatusBar1;
        wxTextCtrl* TextCtrl1;
        wxTextCtrl* TextCtrl2;
        //*)

        void OnCustEvtNum(wxCommandEvent& event);
        void OnCustEvtStr(wxCommandEvent& event);
        void OnCustEvtPkt(MyPacketEvent& evnet);
        DECLARE_EVENT_TABLE()
};

class MyPacketEvent : public wxEvent
{
public:
    MyPacketEvent(wxEventType evtType, int id, const DEF_MYPACKET_TYPE& packet)
        : wxEvent(id, evtType), m_packet(packet)
    {
    }

    DEF_MYPACKET_TYPE GetPacket() const
    {
        return m_packet;
    }

    virtual wxEvent* Clone() const
    {
        return new MyPacketEvent(*this);
    }

private:
    const DEF_MYPACKET_TYPE m_packet;
};
wxDECLARE_EVENT(myEVT_PACKET_UPDATE, MyPacketEvent);

typedef void (wxEvtHandler::*MyPacketEventFuntion)(MyPacketEvent&);
#define MyPackEventHandler(func) wxEVENT_HANDLER_CAST(MyPacketEventFuntion,func)

#define EVT_MY_PACKET_UPDATE(id, func) \
    wx__DECLARE_EVT1(myEVT_PACKET_UPDATE, id, MyPackEventHandler(func))

#endif // WXCUSTEVENTAPPMAIN_H
