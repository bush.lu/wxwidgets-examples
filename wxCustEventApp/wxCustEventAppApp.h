/***************************************************************
 * Name:      wxCustEventAppApp.h
 * Purpose:   Defines Application Class
 * Author:    bradlu ()
 * Created:   2020-04-02
 * Copyright: bradlu ()
 * License:
 **************************************************************/

#ifndef WXCUSTEVENTAPPAPP_H
#define WXCUSTEVENTAPPAPP_H

#include <wx/app.h>

class wxCustEventAppApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // WXCUSTEVENTAPPAPP_H
