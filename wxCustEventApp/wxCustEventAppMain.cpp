/***************************************************************
 * Name:      wxCustEventAppMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    bradlu ()
 * Created:   2020-04-02
 * Copyright: bradlu ()
 * License:
 * Refer: 	  https://docs.wxwidgets.org/trunk/overview_events.html#overview_events_custom
 **************************************************************/

#include "wxCustEventAppMain.h"
#include <wx/msgdlg.h>

//(*InternalHeaders(wxCustEventAppFrame)
#include <wx/font.h>
#include <wx/intl.h>
#include <wx/string.h>
//*)

#include <wx/statline.h>

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>


#define DEFINE_MY_CUST_CMD
#define DEFINE_MY_PACKET_EVT

#ifdef DEFINE_MY_CUST_CMD // copy these from wxWidget/sample/event

// this is typically in a header: it just declares MY_EVENT event type
//wxDECLARE_EVENT(wxEVT_MY_CUSTOM_COMMAND, wxCommandEvent);

// define a custom event type (we don't need a separate declaration here but
// usually you would use a matching wxDECLARE_EVENT in a header)
wxDEFINE_EVENT(wxEVT_MY_CUSTOM_COMMAND, wxCommandEvent);

// it may also be convenient to define an event table macro for this event type
#define EVT_MY_CUSTOM_COMMAND(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_MY_CUSTOM_COMMAND, id, wxID_ANY, \
        wxCommandEventHandler(fn), \
        (wxObject *) NULL \
    ),

#endif

#ifdef DEFINE_MY_PACKET_EVT

wxDEFINE_EVENT(myEVT_PACKET_UPDATE, MyPacketEvent);

#endif

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}



//(*IdInit(wxCustEventAppFrame)
const long wxCustEventAppFrame::ID_BUTTON1 = wxNewId();
const long wxCustEventAppFrame::ID_BUTTON2 = wxNewId();
const long wxCustEventAppFrame::ID_BUTTON3 = wxNewId();
const long wxCustEventAppFrame::ID_BUTTON4 = wxNewId();
const long wxCustEventAppFrame::ID_TEXTCTRL2 = wxNewId();
const long wxCustEventAppFrame::ID_BUTTON5 = wxNewId();
const long wxCustEventAppFrame::ID_BUTTON6 = wxNewId();
const long wxCustEventAppFrame::ID_TEXTCTRL1 = wxNewId();
const long wxCustEventAppFrame::idMenuQuit = wxNewId();
const long wxCustEventAppFrame::idMenuAbout = wxNewId();
const long wxCustEventAppFrame::ID_STATUSBAR1 = wxNewId();
//*)
const long wxCustEventAppFrame::ID_MY_CUST_NUM = wxNewId();
const long wxCustEventAppFrame::ID_MY_CUST_STR = wxNewId();
const long wxCustEventAppFrame::ID_MY_CUST_PKT = wxNewId();

BEGIN_EVENT_TABLE(wxCustEventAppFrame,wxFrame)
    //(*EventTable(wxCustEventAppFrame)
    //*)
    EVT_MY_CUSTOM_COMMAND(wxCustEventAppFrame::ID_MY_CUST_STR, wxCustEventAppFrame::OnCustEvtStr)
    EVT_MY_CUSTOM_COMMAND(wxCustEventAppFrame::ID_MY_CUST_NUM, wxCustEventAppFrame::OnCustEvtNum)
    EVT_MY_PACKET_UPDATE(wxCustEventAppFrame::ID_MY_CUST_PKT, wxCustEventAppFrame::OnCustEvtPkt)
END_EVENT_TABLE()

wxCustEventAppFrame::wxCustEventAppFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(wxCustEventAppFrame)
    wxBoxSizer* BoxSizer1;
    wxBoxSizer* BoxSizer2;
    wxBoxSizer* BoxSizer3;
    wxMenu* Menu1;
    wxMenu* Menu2;
    wxMenuBar* MenuBar1;
    wxMenuItem* MenuItem1;
    wxMenuItem* MenuItem2;
    wxStaticBoxSizer* StaticBoxSizer1;
    wxStaticBoxSizer* StaticBoxSizer2;
    wxStaticBoxSizer* StaticBoxSizer3;
    wxStaticBoxSizer* StaticBoxSizer4;

    Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    BoxSizer2 = new wxBoxSizer(wxVERTICAL);
    StaticBoxSizer1 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Event - Number"));
    BtnEvt1 = new wxButton(this, ID_BUTTON1, _("Evt #1"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    StaticBoxSizer1->Add(BtnEvt1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BtnEvt2 = new wxButton(this, ID_BUTTON2, _("Evt #2"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
    StaticBoxSizer1->Add(BtnEvt2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2->Add(StaticBoxSizer1, 1, wxALL|wxEXPAND, 5);
    StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Event - Text"));
    BtnEvtAbc = new wxButton(this, ID_BUTTON3, _("Evt ABC"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON3"));
    StaticBoxSizer2->Add(BtnEvtAbc, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BtnEvtDEF = new wxButton(this, ID_BUTTON4, _("Evt DEF"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON4"));
    StaticBoxSizer2->Add(BtnEvtDEF, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2->Add(StaticBoxSizer2, 1, wxALL|wxEXPAND, 5);
    StaticBoxSizer4 = new wxStaticBoxSizer(wxVERTICAL, this, _("Event - Packet ( length <= 50B )"));
    TextCtrl2 = new wxTextCtrl(this, ID_TEXTCTRL2, _("01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F 20 21 22 23 24 25 26 27 28 29 2A 2B 2C 2D 2E 2F 30 31 32"), wxDefaultPosition, wxSize(370,33), wxTE_NO_VSCROLL|wxTE_MULTILINE, wxDefaultValidator, _T("ID_TEXTCTRL2"));
    wxFont TextCtrl2Font(10,wxFONTFAMILY_SWISS,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,false,_T("Courier New"),wxFONTENCODING_DEFAULT);
    TextCtrl2->SetFont(TextCtrl2Font);
    StaticBoxSizer4->Add(TextCtrl2, 2, wxALL|wxEXPAND, 5);
    BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
    BtnGen = new wxButton(this, ID_BUTTON5, _("Random Gen."), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON5"));
    BoxSizer3->Add(BtnGen, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BtnEvtPkt = new wxButton(this, ID_BUTTON6, _("Update"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON6"));
    BoxSizer3->Add(BtnEvtPkt, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticBoxSizer4->Add(BoxSizer3, 1, wxALL|wxEXPAND, 5);
    BoxSizer2->Add(StaticBoxSizer4, 0, wxALL|wxEXPAND, 5);
    BoxSizer1->Add(BoxSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticBoxSizer3 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Ouput"));
    TextCtrl1 = new wxTextCtrl(this, ID_TEXTCTRL1, _("Click button to send out event..."), wxDefaultPosition, wxDefaultSize, wxTE_AUTO_SCROLL|wxTE_MULTILINE, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    StaticBoxSizer3->Add(TextCtrl1, 1, wxALL|wxEXPAND, 5);
    BoxSizer1->Add(StaticBoxSizer3, 1, wxALL|wxEXPAND, 5);
    SetSizer(BoxSizer1);
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);
    BoxSizer1->Fit(this);
    BoxSizer1->SetSizeHints(this);

    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxCustEventAppFrame::OnBtnEvt1Click);
    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxCustEventAppFrame::OnBtnEvt2Click);
    Connect(ID_BUTTON3,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxCustEventAppFrame::OnBtnEvtAbcClick);
    Connect(ID_BUTTON4,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxCustEventAppFrame::OnBtnEvtDEFClick);
    Connect(ID_BUTTON6,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxCustEventAppFrame::OnBtnEvtPktClick);
    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxCustEventAppFrame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&wxCustEventAppFrame::OnAbout);
    //*)
}

wxCustEventAppFrame::~wxCustEventAppFrame()
{
    //(*Destroy(wxCustEventAppFrame)
    //*)
}

void wxCustEventAppFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void wxCustEventAppFrame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void wxCustEventAppFrame::OnBtnEvt1Click(wxCommandEvent& event)
{
    printf("Enter wxCustEventAppFrame::OnBtnEvt1Click\r\n");

    wxCommandEvent eventCust(wxEVT_MY_CUSTOM_COMMAND);
    eventCust.SetId(wxCustEventAppFrame::ID_MY_CUST_NUM);
    eventCust.SetInt(1);
    wxPostEvent(this, eventCust);

    return;
}

void wxCustEventAppFrame::OnBtnEvt2Click(wxCommandEvent& event)
{
    printf("Enter wxCustEventAppFrame::OnBtnEvt2Click\r\n");

    wxCommandEvent eventCust(wxEVT_MY_CUSTOM_COMMAND);
    eventCust.SetId(wxCustEventAppFrame::ID_MY_CUST_NUM);
    eventCust.SetInt(2);
    wxPostEvent(this, eventCust);

    return;
}

void wxCustEventAppFrame::OnBtnEvtAbcClick(wxCommandEvent& event)
{
    printf("Enter wxCustEventAppFrame::OnBtnEvtAbcClick\r\n");

    wxCommandEvent eventCust(wxEVT_MY_CUSTOM_COMMAND);
    eventCust.SetId(wxCustEventAppFrame::ID_MY_CUST_STR);
    eventCust.SetString(_T("ABC"));
    wxPostEvent(this, eventCust);

    return;
}

void wxCustEventAppFrame::OnBtnEvtDEFClick(wxCommandEvent& event)
{
    printf("Enter wxCustEventAppFrame::OnBtnEvtDEFClick\r\n");

    wxCommandEvent eventCust(wxEVT_MY_CUSTOM_COMMAND);
    eventCust.SetId(wxCustEventAppFrame::ID_MY_CUST_STR);
    eventCust.SetString(_T("DEF"));
    wxPostEvent(this, eventCust);

    return;
}

void wxCustEventAppFrame::OnBtnEvtPktClick(wxCommandEvent& event)
{
    printf("Enter wxCustEventAppFrame::OnBtnEvtPktClick\r\n");
    std::string strPacket;

    for (int8_t ln = 0; ln < TextCtrl2->GetNumberOfLines(); ln++)
    {
        strPacket += TextCtrl2->GetLineText(ln).ToStdString();
    }
    printf("Packet Text Dump:\r\n");
    printf("%s\r\n", strPacket.c_str());

    /*  Refer link:                                                                               **
    **      https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/                      */
    std::istringstream iss(strPacket);
    std::vector<std::string> results((std::istream_iterator<std::string>(iss)),
                                 std::istream_iterator<std::string>());

    DEF_MYPACKET_TYPE packet;
    uint8_t index = 0;
    memset((void*)&packet, 0x0, sizeof(packet));
    for (std::vector<std::string>::iterator it = results.begin(); (it != results.end()) && (index < 50); ++it)
    {
        int tmp;

        tmp = strtol(it->c_str(), NULL, 16);
        packet.data[index] = tmp;
        index++;
    }
    packet.length = index;
    printf("Got %d bytes.\r\n", index);

    MyPacketEvent eventPacket(myEVT_PACKET_UPDATE, wxCustEventAppFrame::ID_MY_CUST_PKT, packet);
    eventPacket.SetEventObject(this);
    wxPostEvent(this, eventPacket);

    return;
}


/*************************************************************************************************/
/** Customize Event Handle                                                                      **/
/*************************************************************************************************/
void wxCustEventAppFrame::OnCustEvtNum(wxCommandEvent& event)
{
    printf("Enter: wxCustEventAppFrame::OnCustEvtNum\r\n");

    wxString outText;
    outText.Printf(_T("\r\nID: %d, NUM: %d"), event.GetId(), event.GetInt());
    TextCtrl1->AppendText(outText);

    return;
}

void wxCustEventAppFrame::OnCustEvtStr(wxCommandEvent& event)
{
    printf("Enter wxCustEventAppFrame::OnCustEvtStr\r\n");

    wxString outText;
    outText.Printf("\r\nID: %d, STRING: %s", event.GetId(), event.GetString());
    TextCtrl1->AppendText(outText);

    return;
}

void wxCustEventAppFrame::OnCustEvtPkt(MyPacketEvent& event)
{
    printf("Enter wxCustEventAppFrame::OnCustEvtPkt\r\n");

    DEF_MYPACKET_TYPE packet = event.GetPacket();

    wxString outText;
    outText.Printf("\r\nID: %d, Dump packet data:\r\n", event.GetId());
    TextCtrl1->AppendText(outText);

    for (uint8_t index = 0; index < packet.length; index++)
    {
        outText.Printf("0x%02x ", packet.data[index]);
        TextCtrl1->AppendText(outText);
    }

    return;
}

