/***************************************************************
 * Name:      wxSocketServerApp.h
 * Purpose:   Defines Application Class
 * Author:    BradLu (bush.lu@gmail.com)
 * Created:   2020-03-18
 * Copyright: BradLu ()
 * License:
 **************************************************************/

#ifndef WXSOCKETSERVERAPP_H
#define WXSOCKETSERVERAPP_H

#include <wx/app.h>

class wxSocketServerApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // WXSOCKETSERVERAPP_H
