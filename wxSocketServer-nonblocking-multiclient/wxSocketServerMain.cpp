/***************************************************************
 * Name:      wxSocketServerMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    BradLu (bush.lu@gmail.com)
 * Created:   2020-03-18
 * Copyright: BradLu ()
 * License:
 **************************************************************/

#include "wxSocketServerMain.h"
#include <wx/msgdlg.h>
#include <wx/socket.h>
#include <wx/event.h>
#include <wx/atomic.h>

//(*InternalHeaders(wxSocketServerDialog)
#include <wx/font.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(wxSocketServerDialog)
const long wxSocketServerDialog::ID_STATICTEXT2 = wxNewId();
const long wxSocketServerDialog::ID_TEXTCTRL1 = wxNewId();
const long wxSocketServerDialog::ID_STATICTEXT3 = wxNewId();
const long wxSocketServerDialog::ID_TEXTCTRL2 = wxNewId();
const long wxSocketServerDialog::ID_START_BTN = wxNewId();
const long wxSocketServerDialog::ID_STOP_BTN = wxNewId();
const long wxSocketServerDialog::ID_STATICTEXT1 = wxNewId();
const long wxSocketServerDialog::ID_BUTTON2 = wxNewId();
const long wxSocketServerDialog::ID_BUTTON1 = wxNewId();
//*)

enum {
    SERVER_ID = 100,
    SOCKET_ID
};

BEGIN_EVENT_TABLE(wxSocketServerDialog,wxDialog)
    //(*EventTable(wxSocketServerDialog)
    //*)
    EVT_SOCKET(SERVER_ID,  wxSocketServerDialog::OnServerEvent)
    EVT_SOCKET(SOCKET_ID,  wxSocketServerDialog::OnSocketEvent)
END_EVENT_TABLE()

#include <wx/listimpl.cpp>
WX_DEFINE_LIST(CList);

wxSocketServerDialog::wxSocketServerDialog(wxWindow* parent,wxWindowID id)
{
    m_sock_server = NULL;

    //(*Initialize(wxSocketServerDialog)
    wxBoxSizer* BoxSizer2;
    wxBoxSizer* BoxSizer3;
    wxBoxSizer* BoxSizer4;
    wxBoxSizer* BoxSizer5;
    wxBoxSizer* BoxSizer6;
    wxBoxSizer* BoxSizer7;

    Create(parent, wxID_ANY, _("wxWidgets app"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("wxID_ANY"));
    BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    BoxSizer7 = new wxBoxSizer(wxVERTICAL);
    BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
    StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Host IP"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
    BoxSizer3->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    HostAddrText = new wxTextCtrl(this, ID_TEXTCTRL1, _("127.0.0.1"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    BoxSizer3->Add(HostAddrText, 2, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer7->Add(BoxSizer3, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxFIXED_MINSIZE, 4);
    BoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
    StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Port"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
    BoxSizer6->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    HostPortText = new wxTextCtrl(this, ID_TEXTCTRL2, _("3555"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
    BoxSizer6->Add(HostPortText, 2, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer7->Add(BoxSizer6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    StartButton = new wxButton(this, ID_START_BTN, _("Start"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_START_BTN"));
    BoxSizer5->Add(StartButton, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StopButton = new wxButton(this, ID_STOP_BTN, _("Stop"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_STOP_BTN"));
    BoxSizer5->Add(StopButton, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer7->Add(BoxSizer5, 1, wxALL|wxEXPAND, 5);
    BoxSizer1->Add(BoxSizer7, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2 = new wxBoxSizer(wxVERTICAL);
    StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Welcome to\nwxWidgets"), wxDefaultPosition, wxSize(146,64), 0, _T("ID_STATICTEXT1"));
    wxFont StaticText1Font = wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT);
    if ( !StaticText1Font.Ok() ) StaticText1Font = wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT);
    StaticText1Font.SetPointSize(20);
    StaticText1->SetFont(StaticText1Font);
    BoxSizer2->Add(StaticText1, 3, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 10);
    BoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    Button2 = new wxButton(this, ID_BUTTON2, _("Quit"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
    BoxSizer4->Add(Button2, 1, wxALL|wxALIGN_TOP, 4);
    Button1 = new wxButton(this, ID_BUTTON1, _("About"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    BoxSizer4->Add(Button1, 1, wxALL|wxALIGN_TOP, 4);
    BoxSizer2->Add(BoxSizer4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer1->Add(BoxSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SetSizer(BoxSizer1);
    BoxSizer1->Fit(this);
    BoxSizer1->SetSizeHints(this);

    Connect(ID_START_BTN,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxSocketServerDialog::OnStartButtonClick);
    Connect(ID_STOP_BTN,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxSocketServerDialog::OnStopButtonClick);
    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxSocketServerDialog::OnQuit);
    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&wxSocketServerDialog::OnAbout);
    //*)

    StopButton->Enable(FALSE);
    StartButton->Enable(TRUE);
}

wxSocketServerDialog::~wxSocketServerDialog()
{
    //(*Destroy(wxSocketServerDialog)
    //*)
}

void wxSocketServerDialog::OnQuit(wxCommandEvent& event)
{
    Close();
}

void wxSocketServerDialog::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void wxSocketServerDialog::OnStopButtonClick(wxCommandEvent& event)
{
    // broadcast "close" event to all clients.
    CList::compatibility_iterator it2 = m_clist->GetFirst();
    while(it2 != NULL)
    {
        std::cout << it2 <<"\r\n";
        wxSocketBase* client = it2->GetData();

        uint8_t test_string[] = "Bye bye, close connection.\r\n";
        client->Write(test_string, sizeof(test_string));
        client->Close();
        client->Destroy();

        m_clist->DeleteNode(it2);
        it2 = m_clist->GetFirst();
    }
    m_clist->clear();

    m_sock_server->Close();
    m_sock_server->Destroy();
    m_sock_server = NULL;

    this->StopButton->Enable(FALSE);
    this->StartButton->Enable(TRUE);
}

void wxSocketServerDialog::OnStartButtonClick(wxCommandEvent& event)
{
    if (m_sock_server != NULL)
    {
        m_sock_server->Close();
        m_sock_server->Destroy();
        m_sock_server = NULL;
    }


    wxString localIP = HostAddrText->GetLineText(0); // "127.0.0.1";
    wxString localPort = HostPortText->GetLineText(0); // "3000";

    std::cout << "host Address: " << localIP.char_str() << "\r\n";
    std::cout << "host Port: " << localPort.char_str() << "\r\n";

    wxIPV4address localAddr;
    localAddr.Hostname(localIP);
    localAddr.Service(localPort);

    m_sock_server = new wxSocketServer(localAddr);
    if (! m_sock_server->IsOk())
    {
        std::cout << "new server socket - NG. \r\n";
        return;
    }

    m_sock_server->SetNotify(wxSOCKET_CONNECTION_FLAG);
    m_sock_server->SetEventHandler(*this, SERVER_ID);
    m_sock_server->SetNotify(wxSOCKET_CONNECTION_FLAG);
    m_sock_server->Notify(TRUE);

    m_clist = new CList();

    this->StopButton->Enable(TRUE);
    this->StartButton->Enable(FALSE);
    return;
}

void wxSocketServerDialog::OnServerEvent(wxSocketEvent& event)
{
    wxSocketBase* client = NULL;

    std::cout << "OnServerEvent()\r\n";
    switch(event.GetSocketEvent())
    {
    case wxSOCKET_CONNECTION:
    {
        std::cout << "wxSOCKET_CONNECTION\r\n";

        client = m_sock_server->Accept(FALSE);
        wxIPV4address addr;
        if (!client->GetPeer(addr))
        {
            std::cout << "New connection accepted - NG\r\n";
        }

        m_clist->Append(client);

        client->SetEventHandler(*this, SOCKET_ID);
        client->SetNotify(wxSOCKET_OUTPUT_FLAG | wxSOCKET_LOST_FLAG);
        client->Notify(TRUE);
        break;
    }
    default:
        std::cout << event.GetSocketEvent() << "\r\n";
        break;
    }

    return;
}

void wxSocketServerDialog::OnSocketEvent(wxSocketEvent& event)
{
    wxSocketBase *client = event.GetSocket();

    std::cout << "OnSocketEvent()\r\n";
    switch(event.GetSocketEvent())
    {
    case wxSOCKET_INPUT:
    {
        uint8_t buffer[1024];
        std::cout << "wxSOCKET_INPUT\r\n";

        client->Read(buffer, sizeof(buffer));
        std::cout << "RECV: " << buffer << "\r\n";

        break;
    }
    case wxSOCKET_OUTPUT:
    {
        uint8_t test_string[] = "Hello, I'm wxSocketServer.\r\n";
        client->Write(test_string, sizeof(test_string));

        client->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
        client->Notify(TRUE);
        break;
    }
    case wxSOCKET_LOST:
    {
        std::cout << "wxSOCKET_LOST\r\n";
        std::cout << "client connection lost.\r\n";

        client->Close();
        client->Destroy();
        m_clist->DeleteObject(client);
        break;
    }
    default:
        std::cout << event.GetSocketEvent() << "\r\n";
        break;
    }

    return;
}
