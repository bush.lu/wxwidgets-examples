/***************************************************************
 * Name:      wxSocketServerMain.h
 * Purpose:   Defines Application Frame
 * Author:    BradLu (bush.lu@gmail.com)
 * Created:   2020-03-18
 * Copyright: BradLu ()
 * License:
 **************************************************************/

#ifndef WXSOCKETSERVERMAIN_H
#define WXSOCKETSERVERMAIN_H

//(*Headers(wxSocketServerDialog)
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

#include <wx/socket.h>

WX_DECLARE_LIST(wxSocketBase, CList);

class wxSocketServerDialog: public wxDialog
{
    public:

        wxSocketServerDialog(wxWindow* parent,wxWindowID id = -1);
        virtual ~wxSocketServerDialog();

    private:
        wxSocketServer* m_sock_server;
        CList* m_clist;

        void OnServerEvent(wxSocketEvent& event);
        void OnSocketEvent(wxSocketEvent& event);

        //(*Handlers(wxSocketServerDialog)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnStart(wxCommandEvent& event);
        void OnStop(wxCommandEvent& event);
        void OnStopButtonClick(wxCommandEvent& event);
        void OnStartButtonClick(wxCommandEvent& event);
        //*)

        //(*Identifiers(wxSocketServerDialog)
        static const long ID_STATICTEXT2;
        static const long ID_TEXTCTRL1;
        static const long ID_STATICTEXT3;
        static const long ID_TEXTCTRL2;
        static const long ID_START_BTN;
        static const long ID_STOP_BTN;
        static const long ID_STATICTEXT1;
        static const long ID_BUTTON2;
        static const long ID_BUTTON1;
        //*)

        //(*Declarations(wxSocketServerDialog)
        wxBoxSizer* BoxSizer1;
        wxButton* Button1;
        wxButton* Button2;
        wxButton* StartButton;
        wxButton* StopButton;
        wxStaticText* StaticText1;
        wxStaticText* StaticText2;
        wxStaticText* StaticText3;
        wxTextCtrl* HostAddrText;
        wxTextCtrl* HostPortText;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // WXSOCKETSERVERMAIN_H
