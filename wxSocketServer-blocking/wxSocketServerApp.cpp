/***************************************************************
 * Name:      wxSocketServerApp.cpp
 * Purpose:   Code for Application Class
 * Author:    BradLu (bush.lu@gmail.com)
 * Created:   2020-03-18
 * Copyright: BradLu ()
 * License:
 **************************************************************/

#include "wxSocketServerApp.h"

//(*AppHeaders
#include "wxSocketServerMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(wxSocketServerApp);

bool wxSocketServerApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	wxSocketServerDialog Dlg(0);
    	SetTopWindow(&Dlg);
    	Dlg.ShowModal();
    	wxsOK = false;
    }
    //*)
    return wxsOK;

}
