/***************************************************************
 * Name:      wxSerialConsoleMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    bradlu ()
 * Created:   2020-03-25
 * Copyright: bradlu ()
 * License:
 * Reference: https://docs.microsoft.com/en-us/previous-versions/ff802693(v=msdn.10)?redirectedfrom=MSDN
 *
 **************************************************************/
//

#include "mySerial.h"

mySerial::mySerial()
{
    hComm = INVALID_HANDLE_VALUE;
    port = "\\\\.\\COM1";

    SetBaudrate(115200);
    SetDataSize(8);
    SetParity('N');
    SetStopbits(1.0);
    return;
}

mySerial::mySerial(string device, long baudrate, long data_sz, char parity, float stopbits)
{
    hComm = INVALID_HANDLE_VALUE;
    port = device;

    SetBaudrate(baudrate);
    SetDataSize(data_sz);
    SetParity(parity);
    SetStopbits(stopbits);
    return;
}

mySerial::~mySerial()
{
    Close();
    return;
}

uint8_t mySerial::Open()
{
    hComm = CreateFileA (port.c_str(),
                        GENERIC_READ | GENERIC_WRITE,
                        0,
                        0,
                        OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                        0);
    if (hComm == INVALID_HANDLE_VALUE)
        return MYERR_NG;

    // Flush / Abort TX and RX.
    PurgeComm(hComm, PURGE_RXCLEAR | PURGE_TXCLEAR);

    DCB dcbBak;
    DCB dcbNew;

    FillMemory(&dcbBak, sizeof(dcbBak), 0);
    dcbBak.DCBlength = sizeof(dcbBak);

    GetCommState(hComm, &dcbBak);
    dcbNew = dcbBak;
    dcbNew.Parity = parity;
    dcbNew.BaudRate = baudrate;
    dcbNew.ByteSize = data_sz;
    dcbNew.StopBits = stopbits;
    dcbNew.fNull = TRUE;

    // current not support flow control.
//    dcbNew.fOutxCtsFlow = FALSE;
//    dcbNew.fOutxDsrFlow = FALSE;
//    dcbNew.fOutX = FALSE;
//    dcbNew.fDtrControl = DTR_CONTROL_DISABLE;
//    dcbNew.fRtsControl = RTS_CONTROL_DISABLE;

    SetCommState(hComm, &dcbNew);
    //if (SetCommState(hComm, &dcbNew) != 0)
    //    return 2;
    GetCommState(hComm, &dcbNew);

    printf("Baudrate: %d\r\n", dcbNew.BaudRate);
    printf("Bytesize: %d\r\n", dcbNew.ByteSize);
    printf("Parity: %d\r\n", dcbNew.Parity);
    printf("Stopbits: %d\r\n", dcbNew.StopBits);

    osReader = {0};
    osReader.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    osWrite = {0};
    osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    if ((osReader.hEvent == NULL) || (osWrite.hEvent == NULL))
        return 3;

    GetCommTimeouts(hComm, &timeouts);

    timeouts.ReadIntervalTimeout = 20;
    timeouts.ReadTotalTimeoutMultiplier = 10;
    timeouts.ReadTotalTimeoutConstant = 100;
    timeouts.WriteTotalTimeoutMultiplier = 100;
    timeouts.WriteTotalTimeoutConstant = 100;

    SetCommTimeouts(hComm, &timeouts);
    //if (SetCommTimeouts(hComm, &timeouts) != 0)
    //    return 4;

    return MYERR_OK;
}

uint8_t mySerial::Close()
{
    return MYERR_OK;
}

uint8_t mySerial::SetPort(string device)
{
    port = device;
    return MYERR_OK;
}

uint8_t mySerial::SetBaudrate(long baud)
{
    switch(baud)
    {
    case 110:
    case 300:
    case 600:
    case 1200:
    case 2400:
    case 4800:
    case 9600:
    case 14400:
    case 19200:
    case 38400:
    case 56000:
    case 57600:
    case 115200:
    case 128000:
    case 256000:
    {
        baudrate = baud;
        break;
    }
    default:
        baudrate = CBR_115200;
        break;
    }
    return MYERR_OK;
}
uint8_t mySerial::SetDataSize(long sz)
{
    if ((sz >= 4) && (sz <= 8))
    {
        data_sz = sz;
    }
    else
    {
        data_sz = 8;
    }
    return MYERR_OK;
}

uint8_t mySerial::SetParity(char type)
{
    switch(type)
    {
    case 'N':
    case 'n':
    {
        parity = NOPARITY;
        break;
    }
    case 'O':
    case 'o':
    {
        parity = ODDPARITY;
        break;
    }
    case 'E':
    case 'e':
    {
        parity = EVENPARITY;
        break;
    }
    case 'M':
    case 'm':
    {
        parity = MARKPARITY;
        break;
    }
    case 'S':
    case 's':
    {
        parity = SPACEPARITY;
        break;
    }
    default:
        parity = NOPARITY;
        break;
    }
    return MYERR_OK;
}

uint8_t mySerial::SetStopbits(float bits)
{
    if (bits == 1)
    {
        stopbits = ONESTOPBIT;
    }
    else if (bits == 1.5)
    {
        stopbits = ONE5STOPBITS;
    }
    else if (bits == 2)
    {
        stopbits = TWOSTOPBITS;
    }
    else
    {
        stopbits = ONESTOPBIT;
    }
    return MYERR_OK;
}

uint8_t mySerial::SetRTS(bool value)
{
    return MYERR_OK;
}
uint8_t mySerial::SetDTR(bool value)
{
    return MYERR_OK;
}

uint8_t mySerial::Read(uint8_t* buff, uint8_t* size)
{
    uint8_t ret = MYERR_OK;
    uint8_t buff_sz = *size;

    DWORD dwCommEvent;
    DWORD dwRead;
    uint8_t*  chRead = buff;
    OVERLAPPED osReader = {0};

    *size = 0;
    if (!SetCommMask(hComm, EV_RXCHAR))
    {   // Error setting communications event mask.
        ret = MYERR_NG;
    }
    else
    {
        if (WaitCommEvent(hComm, &dwCommEvent, NULL))
        {
            ret = MYERR_NG;
            do {
                if (ReadFile(hComm, chRead, 1, &dwRead, &osReader))
                {
                    *size += dwRead;
                    chRead++;
                    ret = MYERR_OK;
                }
            } while (dwRead);
        }
    }
    return ret;
}

uint8_t mySerial::Readchar(char* ch)
{
    uint8_t ret = MYERR_OK;
    DWORD dwRead;
    BOOL fWaitingOnRead = FALSE;

    if (fWaitingOnRead == FALSE)
    {
        if (ReadFile(hComm, ch, 1, &dwRead, &osReader) == FALSE)
        {
            if (GetLastError() != ERROR_IO_PENDING)
            {   /* should be handed here. */
                ret = MYERR_NG;
            }
            else
                fWaitingOnRead = TRUE;
        }
        else
        {
            if (dwRead == 1)
            {
                printf ("read: %x\r\n", *ch);
                ret = MYERR_OK;
                return ret;
            }
        }
    } /* end of if (fWaitingOnRead == FLASE) */

    if (fWaitingOnRead == TRUE)
    {
        DWORD dwRes;
        dwRes = WaitForSingleObject(osReader.hEvent, MYDEF_SERAIL_READ_TIMEOUT);
        switch(dwRes)
        {
        case WAIT_OBJECT_0:
        {
            if (GetOverlappedResult(hComm, &osReader, &dwRead, FALSE) == FALSE)
            { /* some error happen should report it. */
                ret = MYERR_UNKNOW;
            }
            else
            {
                if (dwRead != 0)
                {
                    ret = MYERR_OK;
                    fWaitingOnRead = FALSE; // not necessary
                    printf ("read: %x\r\n", *ch);
                }
                else
                {
                    ret = MYERR_EMPTY;
                }
            } /* end of if (GetOverlappedResult(hComm, &osReader, &dwRead, FALSE) == FALSE) else */
            break;
        }
        case WAIT_TIMEOUT:
        {   /* nothing be received this time, retry? */
            ret = MYERR_TIMEOUT;
            break;
        }
        default:
            break;
        } /* end of switch(dwRes) */
    } /* end of if (fWaitingOnRead == TRUE) */

    return ret;
}

uint8_t mySerial::Write(uint8_t* buff, uint8_t* psize)
{
    uint8_t ret = MYERR_OK;

    OVERLAPPED osWrite = {0};
    DWORD dwWritten;
    DWORD dwRes;
    DWORD dwToWrite = (DWORD) *psize;

    if (!WriteFile(hComm, buff, dwToWrite, &dwWritten, &osWrite))
    {
        if (GetLastError() == ERROR_IO_PENDING)
        {
            DWORD dwRes;

            dwRes = WaitForSingleObject(osWrite.hEvent, 100);

            switch(dwRes)
            {
            case WAIT_OBJECT_0:
                if (!GetOverlappedResult(hComm, &osWrite, &dwWritten, FALSE))
                {
                    *psize = 0;
                    ret = MYERR_NG;
                }

                if (dwToWrite != dwWritten)
                {
                    *psize = dwWritten;
                    ret = MYERR_NG;
                }
                break;
            }
        }
    }
    return ret;
}


