/***************************************************************
 * Name:      wxSerialConsoleApp.cpp
 * Purpose:   Code for Application Class
 * Author:    bradlu ()
 * Created:   2020-03-25
 * Copyright: bradlu ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "wxSerialConsoleApp.h"
#include "wxSerialConsoleMain.h"

IMPLEMENT_APP(wxSerialConsoleApp);

bool wxSerialConsoleApp::OnInit()
{
    wxSerialConsoleFrame* frame = new wxSerialConsoleFrame(0L, _("wxWidgets Application Template"));
    frame->SetIcon(wxICON(aaaa)); // To Set App Icon
    frame->Show();
    
    return true;
}
