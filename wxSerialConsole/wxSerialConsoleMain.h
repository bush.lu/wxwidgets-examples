/***************************************************************
 * Name:      wxSerialConsoleMain.h
 * Purpose:   Defines Application Frame
 * Author:    bradlu ()
 * Created:   2020-03-25
 * Copyright: bradlu ()
 * License:
 **************************************************************/

#ifndef WXSERIALCONSOLEMAIN_H
#define WXSERIALCONSOLEMAIN_H

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "wxSerialConsoleApp.h"
#include "mySerial.h"

enum
{
    idMenuQuit = 1000,
    idMenuAbout,
    idTxtRecv,
    idTxtSend,
    idBtnSend,
    idTimerPolling,
    idThreadCtrl

};

typedef struct _UINT8_PACKET
{
    uint8_t data[50];
    uint8_t len;
} UINT8_PACKET;

class wxSerialWorkerThread;

class wxSerialConsoleFrame: public wxFrame
{
public:
    wxSerialConsoleFrame(wxFrame *frame, const wxString& title);
    ~wxSerialConsoleFrame();
    mySerial*   m_comDev;

private:
    wxSerialWorkerThread* m_workerThrd;
    wxTextCtrl* m_txtRecv;
    wxTextCtrl* m_txtSend;
    wxButton*   m_btnSend;
    wxTimer*    m_polling;

    void OnClose(wxCloseEvent& event);
    void OnQuit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnPolling(wxTimerEvent& event);
    void OnSend(wxCommandEvent& event);
    void OnWorkerEvent(wxThreadEvent& event);

    DECLARE_EVENT_TABLE()
};



#endif // WXSERIALCONSOLEMAIN_H
