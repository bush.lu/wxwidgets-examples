/***************************************************************
 * Name:      wxSerialConsoleApp.h
 * Purpose:   Defines Application Class
 * Author:    bradlu ()
 * Created:   2020-03-25
 * Copyright: bradlu ()
 * License:
 **************************************************************/

#ifndef WXSERIALCONSOLEAPP_H
#define WXSERIALCONSOLEAPP_H

#include <wx/app.h>

class wxSerialConsoleApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // WXSERIALCONSOLEAPP_H
