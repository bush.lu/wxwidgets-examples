/***************************************************************
 * Name:      wxSerialConsoleMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    bradlu ()
 * Created:   2020-03-25
 * Copyright: bradlu ()
 * License:
 * Reference: https://docs.microsoft.com/en-us/previous-versions/ff802693(v=msdn.10)?redirectedfrom=MSDN
 *
 **************************************************************/
#ifndef MYSERIAL_H
#define MYSERIAL_H

#include <stdio.h>
#include <iostream>
#include <string>

#ifdef _WIN32
	#include <windows.h>
#else
    #error "not define _WIN32"
#endif

#define MYDEF_SERAIL_READ_TIMEOUT   (500) /* ms */

typedef enum _MY_ERROR_LST
{
    MYERR_OK        = 0,
    MYERR_NG        = 1,
    MYERR_PARA      = 2,
    MYERR_TIMEOUT   = 3,
    MYERR_EMPTY     = 4,

    MYERR_UNKNOW    = 255
} MY_ERROR_LST;

using namespace std;

class mySerial
{
    public:
        mySerial();
        mySerial(string device, long baudrate, long data_sz, char parity, float stopbits);
        virtual ~mySerial();

        uint8_t Open();
        uint8_t Close();
        uint8_t Readchar(char* ch);
        uint8_t Read(uint8_t* buff, uint8_t* size);
        uint8_t Writechar(char ch);
        uint8_t Write(uint8_t* data, uint8_t* size);
        uint8_t IsOpen();

        uint8_t SetPort(string device);
        uint8_t SetBaudrate(long baud);
        uint8_t SetDataSize(long sz);
        uint8_t SetParity(char type);
        uint8_t SetStopbits(float bits);
        uint8_t SetRTS(bool value);
        uint8_t SetDTR(bool value);

    protected:

    private:
        // basic serial port parameters.
        char rxch;
        string port;
        long baudrate;
        long data_sz;
        uint8_t parity;
        uint8_t stopbits;

        // parameters for WIN32 APIs.
        HANDLE hComm;
        OVERLAPPED osReader;
        OVERLAPPED osWrite;
        BOOL waiting;
        COMMTIMEOUTS timeouts;
};

#endif // MYSERIAL_H
