/***************************************************************
 * Name:      wxSerialConsoleMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    bradlu ()
 * Created:   2020-03-25
 * Copyright: bradlu ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "wxSerialConsoleMain.h"

class wxSerialWorkerThread: public wxThread
{
public:
    wxSerialWorkerThread(void* frame);
    // thread execution starts here
    virtual void *Entry();

    // called when the thread exits - whether it terminates normally or is
    // stopped with Delete() (but not when it is Kill()ed!)
    virtual void OnExit();

private:
    wxSerialConsoleFrame*    m_frame;
};

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

BEGIN_EVENT_TABLE(wxSerialConsoleFrame, wxFrame)
    EVT_CLOSE(wxSerialConsoleFrame::OnClose)
    EVT_MENU(idMenuQuit, wxSerialConsoleFrame::OnQuit)
    EVT_MENU(idMenuAbout, wxSerialConsoleFrame::OnAbout)
    EVT_BUTTON(idBtnSend, wxSerialConsoleFrame::OnSend)
    EVT_TIMER(idTimerPolling, wxSerialConsoleFrame::OnPolling)
    EVT_THREAD(idThreadCtrl, wxSerialConsoleFrame::OnWorkerEvent)
END_EVENT_TABLE()

wxSerialConsoleFrame::wxSerialConsoleFrame(wxFrame *frame, const wxString& title)
    : wxFrame(frame, -1, title)
{
#if wxUSE_MENUS
    // create a menu bar
    wxMenuBar* mbar = new wxMenuBar();
    wxMenu* fileMenu = new wxMenu(_T(""));
    fileMenu->Append(idMenuQuit, _("&Quit\tAlt-F4"), _("Quit the application"));
    mbar->Append(fileMenu, _("&File"));

    wxMenu* helpMenu = new wxMenu(_T(""));
    helpMenu->Append(idMenuAbout, _("&About\tF1"), _("Show info about this application"));
    mbar->Append(helpMenu, _("&Help"));

    SetMenuBar(mbar);
#endif // wxUSE_MENUS

    m_txtRecv = new wxTextCtrl(this, idTxtRecv, _T(""), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
    m_txtSend = new wxTextCtrl(this, idTxtSend, _T(""), wxDefaultPosition, wxDefaultSize, 0);
    m_btnSend = new wxButton(this, idBtnSend, _T("Send"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("BTN_SEND"));

    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(m_txtRecv, wxSizerFlags(1).Expand());

    wxBoxSizer *sendBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	sendBoxSizer->Add(m_txtSend, wxSizerFlags(1).Expand());
	sendBoxSizer->Add(m_btnSend, wxSizerFlags().Expand());

    sizer->Add(sendBoxSizer, wxSizerFlags().Expand());
    SetSizer(sizer);
    SetSize(600, 350);

    m_comDev = new mySerial();
    m_polling = new wxTimer(this, idTimerPolling);

#if wxUSE_STATUSBAR
    // create a status bar with some information about the used wxWidgets version
    CreateStatusBar(2);
    SetStatusText(_("Hello Code::Blocks user!"),0);
    SetStatusText(wxbuildinfo(short_f), 1);
#endif // wxUSE_STATUSBAR

    m_comDev->SetPort("\\\\.\\COM3");
    m_comDev->SetBaudrate(115200);
    m_comDev->SetDataSize(8);
    m_comDev->SetParity('N');
    m_comDev->SetStopbits(1);

    uint8_t ret = 0;
    ret = m_comDev->Open();
    if (ret == 0)
    {
        std::cout << "\\\\.\\COM3 is opened\r\n";
    //    m_polling->Start(500, FALSE);
        m_workerThrd = new wxSerialWorkerThread((void*)this);
        if ( m_workerThrd->Create() == wxTHREAD_NO_ERROR )
            m_workerThrd->Run();
        else
            std::cout << "Can't create thread!" << endl;
    }
    else
    {
        printf("COM open failed, Err: %d\r\n", ret);
    }
}


wxSerialConsoleFrame::~wxSerialConsoleFrame()
{
}

void wxSerialConsoleFrame::OnClose(wxCloseEvent &event)
{
    Destroy();
}

void wxSerialConsoleFrame::OnQuit(wxCommandEvent &event)
{
    Destroy();
}

void wxSerialConsoleFrame::OnAbout(wxCommandEvent &event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void wxSerialConsoleFrame::OnPolling(wxTimerEvent& event)
{
    uint8_t cdata = 0;
    uint8_t ret = 0;

    ret = m_comDev->Readchar((char*)&cdata);
    if (ret == 0)
    {   // update data on window
        m_txtRecv->AppendText(wxString::Format(_T("%02x "), cdata));
    }
    else
    {
        if (ret == MYERR_EMPTY)
        {
        }
        else
        {
            printf("COM Readchar Err: %d\r\n", ret);
        }
    } /* end of if (ret == 0) else */
}

void wxSerialConsoleFrame::OnSend(wxCommandEvent& event)
{
    std::string str;
    str.assign(m_txtSend->GetValue());

    uint8_t buff[100];
    uint8_t* ptr = (uint8_t*) str.c_str();
    uint8_t remain =str.length();
    uint8_t sz = 0;

    while (remain > 0)
    {
        memset(buff, 0, sizeof(buff));
        ptr += sz;

        if (remain > sizeof(buff))
        {
            sz = sizeof(buff);
            remain -= sizeof(buff);
        }
        else
        {
            sz = remain;
            remain = 0;
        }

        memcpy (buff, ptr, sz);
        //std::cout << (char*) buff << endl;

        uint8_t ret = m_comDev->Write(buff, &sz);
        if (ret != 0)
        {
            printf("COM Write error: %d", ret);
            break;
        }
    }
    m_txtSend->Clear();
    return;
}

void wxSerialConsoleFrame::OnWorkerEvent(wxThreadEvent& event)
{
    UINT8_PACKET recvdata;

    recvdata = event.GetPayload<UINT8_PACKET>();
    for (uint8_t indx = 0; indx < recvdata.len; indx++)
    {
         m_txtRecv->AppendText(wxString::Format(_T("%02x "), recvdata.data[indx]));
    }

}

wxSerialWorkerThread::wxSerialWorkerThread(void* frame) : wxThread()
{
    m_frame = (wxSerialConsoleFrame*)frame;
}

void* wxSerialWorkerThread::Entry()
{
    if ( TestDestroy() )
        return NULL;

    while(! TestDestroy())
    {
        uint8_t ret;
        UINT8_PACKET recvdata;
        recvdata.len = sizeof(recvdata.data);

        memset((void*)recvdata.data, 0, recvdata.len);
        ret = m_frame->m_comDev->Read(&recvdata.data[0], &recvdata.len);
        if (ret == MYERR_OK)
        {
            if (recvdata.len > 0)
            {   // create any type of command event here
                wxThreadEvent event( wxEVT_THREAD, idThreadCtrl);
                event.SetPayload(recvdata);

                // send in a thread-safe way
                wxQueueEvent( m_frame, event.Clone() );
            }
        } /* end of if (ret == MYERR_OK) */
    } /* end of while(! TestDestroy()) */

    return NULL;
}

void wxSerialWorkerThread::OnExit()
{
    return;
}
