#!/usr/bin/python
import socket

def connectServer():
    addr = '127.0.0.1'
    port = 3000
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((addr, port))
    data = sock.recv(1024)
    print (data)
    data = "Hello, I'm python client. Bye Bye!\r\n"
    sock.send(str.encode(data))
    sock.close()


if __name__ == '__main__':
    connectServer()
